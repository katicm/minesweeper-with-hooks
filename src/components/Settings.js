import React, { useState, useContext } from "react";
import { ParamContext } from "../App";

const Settings = () => {
  const { parameters, setParameters } = useContext(ParamContext);
  const [newParam, setNewParam] = useState(parameters);
  const { xSize, ySize, bombNum } = newParam;

  const handleSubmit = e => {
    e.preventDefault();
    if (
      !(isNaN(xSize) || isNaN(ySize) || isNaN(bombNum)) &&
      xSize * ySize > bombNum &&
      bombNum > 0
    ) {
      setParameters(newParam);
    }
  };
  const handleChange = e => {
    setNewParam(getParameters(e.target.value));
    setParameters(getParameters(e.target.value));
  };

  const getParameters = e => {
    switch (e) {
      case "beginner":
        return { xSize: 9, ySize: 9, bombNum: 10, mode: "beginner" };
      case "intermediate":
        return { xSize: 16, ySize: 16, bombNum: 40, mode: "intermediate" };
      case "expert":
        return { xSize: 16, ySize: 30, bombNum: 99, mode: "expert" };
      default:
        return { ...parameters, mode: "custom" };
    }
  };

  return (
    <React.Fragment>
      <div className="sector_title">GAME SETTINGS </div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <div className="container_radio">
          <label className="settings_radio">
            <input
              type="radio"
              name="game"
              value="beginner"
              onChange={handleChange}
              checked={parameters.mode === "beginner"}
            />
            Beginner
            <span className="checkmark" />
          </label>
          <label className="settings_radio">
            <input
              type="radio"
              name="game"
              value="intermediate"
              onChange={handleChange}
              checked={parameters.mode === "intermediate"}
            />
            Intermediate
            <span className="checkmark" />
          </label>
          <label className="settings_radio">
            <input
              type="radio"
              name="game"
              value="expert"
              onChange={handleChange}
              checked={parameters.mode === "expert"}
            />
            Expert
            <span className="checkmark" />
          </label>
          <label className="settings_radio">
            <input
              type="radio"
              name="game"
              value="custom"
              onChange={handleChange}
              checked={parameters.mode === "custom"}
            />
            Custom
            <span className="checkmark" />
          </label>
        </div>
        <div className="form_container">
          <form
            style={{ display: "flex", flexDirection: "column" }}
            onSubmit={handleSubmit}
          >
            <div className="param_container">
              <label className="param_label">Columns:</label>
              <input
                value={xSize}
                type="text"
                className="param_input"
                disabled={parameters.mode !== "custom"}
                onChange={e => {
                  setNewParam({ ...newParam, xSize: e.target.value });
                }}
              />
            </div>
            <div className="param_container">
              <label className="param_label">Rows:</label>
              <input
                value={ySize}
                type="text"
                className="param_input"
                disabled={parameters.mode !== "custom"}
                onChange={e => {
                  setNewParam({ ...newParam, ySize: e.target.value });
                }}
              />
            </div>
            <div className="param_container">
              <label className="param_label">Bombs:</label>
              <input
                value={bombNum}
                type="text"
                className="param_input"
                disabled={parameters.mode !== "custom"}
                onChange={e => {
                  setNewParam({ ...newParam, bombNum: e.target.value });
                }}
              />
            </div>
            <input
              className="form_submit"
              type={newParam.mode === "custom" ? "submit" : "hidden"}
              value="SET TABLE"
            />
          </form>
        </div>
      </div>
    </React.Fragment>
  );
};
export default Settings;
