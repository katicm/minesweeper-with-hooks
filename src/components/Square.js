import React, { useEffect, useRef } from "react";
import flag from "../media/flag.png";
import bomb from "../media/bomb.png";
import red_bomb from "../media/red_bomb.png";
import xbomb from "../media/xbomb.png";

const setColor = bombsInRange => {
  switch (bombsInRange) {
    case 0:
      return "transparent";
    case 1:
      return "#1a75ff";
    case 2:
      return "#009900";
    case 3:
      return "#e60000";
    case 4:
      return "#000080";
    default:
      return "#660000";
  }
};

const Square = props => {
  const { x, y, isBomb, bombsInRange, isClicked, isFlagged } = props.state;
  const {
    checkZero,
    dispatch,
    flagChange,
    gameStatus: { ended, type },
    setGameStatus
  } = props;
  const didMount = useRef(false);

  const handleLClick = e => {
    if (!isFlagged && !ended) {
      if (isBomb) {
        setGameStatus({ ended: true, type: "lost" });
      }
      dispatch({ type: "LCLICK", payload: { x, y } });
    }
  };
  const handleRClick = () => {
    if (!ended) {
      flagChange(isFlagged);
      dispatch({ type: "RCLICK", payload: { x, y } });
    }
  };
  const handleClick = e => {
    if (e.type === "mousedown" && !ended) {
      setGameStatus({ ended: false, type: "click" });
    } else if (!ended) {
      setGameStatus({ ended, type: ended ? "won" : "run" });
    }
  };

  useEffect(() => {
    if (!didMount.current) {
      didMount.current = true;
    } else {
      if (isClicked && bombsInRange === 0) {
        checkZero({ xPos: x, yPos: y });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isClicked]);
  const key = `${isClicked}-${isFlagged}-${isBomb}`;
  return (
    <td>
      <button
        className="button_square"
        onClick={handleLClick}
        onContextMenu={handleRClick}
        onMouseDown={handleClick}
        onMouseUp={handleClick}
        disabled={isClicked ? true : false}
        style={{
          color: setColor(bombsInRange)
        }}
      >
        {
          {
            "true-false-false": bombsInRange,
            "false-true-true": <img src={flag} alt="F"></img>,
            "false-false-true": ended ? (
              type === "won" ? (
                <img src={flag} alt="F"></img>
              ) : (
                <img src={bomb} alt="B"></img>
              )
            ) : null,
            "true-false-true": (
              <img className="image_bomb" src={red_bomb} alt="R"></img>
            ),
            "false-true-false": ended ? (
              <img src={xbomb} alt="X"></img>
            ) : (
              <img src={flag} alt="F"></img>
            )
          }[key]
        }
      </button>
    </td>
  );
};
export default Square;
