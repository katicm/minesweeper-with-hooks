import React, { useContext } from "react";
import Square from "./Square";
import GameCenter from "./GameCenter";
import useTable from "./useTable";
import Modal from "./Modal";
import { ParamContext } from "../App";

const Board = () => {
  const { parameters } = useContext(ParamContext);
  const [
    state,
    dispatch,
    flagNum,
    setFlagNum,
    time,
    gameStatus,
    setGameStatus,
    newGame
  ] = useTable(parameters);

  const checkZero = ({ xPos, yPos }) => {
    for (let x = xPos - 1; x <= xPos + 1; x++) {
      for (let y = yPos - 1; y <= yPos + 1; y++) {
        try {
          if (!state.table[x][y].isClicked && !state.table[x][y].isFlagged) {
            dispatch({ type: "LCLICK", payload: { x, y } });
          }
        } catch (e) {}
      }
    }
  };
  const flagChange = isFlagged => {
    !isFlagged ? setFlagNum(flagNum - 1) : setFlagNum(flagNum + 1);
  };

  return (
    <React.Fragment>
      <GameCenter
        type={gameStatus.type}
        flagNum={flagNum}
        time={time}
        newGame={newGame}
      ></GameCenter>
      <Modal type={gameStatus.type} time={time} />
      <table cellSpacing="0.5" cellPadding="1" align="center">
        <tbody>
          {state.table.map((row, i) => (
            <tr key={i}>
              {row.map((cell, j) => (
                <Square
                  key={"" + i + j}
                  state={cell}
                  dispatch={dispatch}
                  checkZero={checkZero}
                  flagChange={flagChange}
                  gameStatus={gameStatus}
                  setGameStatus={setGameStatus}
                ></Square>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </React.Fragment>
  );
};
export default Board;
