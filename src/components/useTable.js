import { useState, useReducer, useEffect } from "react";

const isBomb = ({ x, y, bombsMap }) => {
  return bombsMap.some(bomb => bomb.x === x && bomb.y === y);
};
const generateBombs = ({ xSize, ySize, bombNum }) => {
  let bombsMap = [];
  while (bombsMap.length < bombNum) {
    let x = Math.floor(Math.random() * xSize);
    let y = Math.floor(Math.random() * ySize);

    if (!bombsMap.some(bomb => bomb.x === x && bomb.y === y)) {
      bombsMap.push({ x, y });
    }
  }
  return bombsMap;
};
const bombsInRange = ({ xPos, yPos, bombsMap }) => {
  let count = 0;
  for (let x = xPos - 1; x <= xPos + 1; x++) {
    for (let y = yPos - 1; y <= yPos + 1; y++) {
      if (isBomb({ x, y, bombsMap })) {
        count++;
      }
    }
  }
  return isBomb({ x: xPos, y: yPos, bombsMap }) ? count : count;
};
const generateTable = parameters => {
  let { xSize, ySize, bombNum } = parameters;
  let table = [];
  let bombsMap = generateBombs({ xSize, ySize, bombNum });
  for (let x = 0; x < xSize; x++) {
    let row = [];
    for (let y = 0; y < ySize; y++) {
      row.push({
        x,
        y,
        isBomb: isBomb({ x, y, bombsMap }),
        bombsInRange: bombsInRange({ xPos: x, yPos: y, bombsMap }),
        isClicked: false,
        isFlagged: false
      });
    }
    table.push(row);
  }
  return { table: table };
};
const tableReducer = (state, action) => {
  switch (action.type) {
    case "RCLICK":
      return {
        table: state.table.map(j =>
          j.map(k =>
            action.payload.x === k.x && action.payload.y === k.y
              ? { ...k, isFlagged: !k.isFlagged }
              : k
          )
        )
      };
    case "LCLICK":
      return {
        table: state.table.map(j =>
          j.map(k =>
            action.payload.x === k.x && action.payload.y === k.y
              ? { ...k, isClicked: true }
              : k
          )
        )
      };
    case "NEW_GAME":
      return action.payload;
    default:
      return state;
  }
};

const useTable = parameters => {
  const [state, dispatch] = useReducer(tableReducer, parameters, generateTable);
  const [flagNum, setFlagNum] = useState(parameters.bombNum);
  const [time, setTime] = useState(1);
  const [gameStatus, setGameStatus] = useState({ ended: false, type: "run" });

  // array.some()
  useEffect(() => {
    const bombLeft = [];
    state.table.map(row =>
      row.map(cell =>
        !cell.isBomb && !cell.isClicked ? bombLeft.push(cell) : null
      )
    );
    if (bombLeft.length === 0) {
      setGameStatus({ ended: true, type: "won" });
    }
  }, [flagNum, state.table]);
  useEffect(() => {
    if (!gameStatus.ended) {
      const stopwatch = setInterval(() => {
        setTime(prevTime => prevTime + 1);
      }, 1000);
      return () => {
        clearInterval(stopwatch);
      };
    }
  }, [time, gameStatus.ended]);

  const newGame = () => {
    dispatch({ type: "NEW_GAME", payload: generateTable(parameters) });
    setGameStatus({ ended: false, type: "run" });
    setFlagNum(parameters.bombNum);
    setTime(0);
  };

  return [
    state,
    dispatch,
    flagNum,
    setFlagNum,
    time,
    gameStatus,
    setGameStatus,
    newGame
  ];
};
export default useTable;
