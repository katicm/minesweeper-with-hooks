import React, { useState, useEffect } from "react";
import axios from "axios";

const Records = () => {
  const [records, setRecords] = useState([]);
  const [mode, setMode] = useState("beginner");

  useEffect(() => {
    axios
      .get(`https://minesweeper-records.firebaseio.com/records/${mode}.json`)
      .then(result => {
        const data = [];
        for (const key in result.data) {
          data.push({
            id: key,
            name: result.data[key].name,
            date: result.data[key].date,
            time: result.data[key].time
          });
        }
        data.sort((a, b) => a.time - b.time);
        setRecords(data);
      });
  }, [mode]);

  return (
    <React.Fragment>
      <div className="sector_title">10 FASTEST GAMES</div>
      <div className="ranking_quote">
        "If you don't see yourself as a winner,<br></br> then you cannot perform
        as a winner."
      </div>
      <div className="tab_table">
        <button
          className={mode[0] === "b" ? "cli_tab" : "tab_but"}
          onClick={() => setMode("beginner")}
        >
          Beginner
        </button>
        <button
          className={mode[0] === "i" ? "cli_tab" : "tab_but"}
          onClick={() => setMode("intermediate")}
        >
          Intermediate
        </button>
        <button
          className={mode[0] === "e" ? "cli_tab" : "tab_but"}
          onClick={() => setMode("expert")}
        >
          Expert
        </button>
        <table align="center" className="ranking_table">
          <thead>
            <tr>
              <th>Rank</th>
              <th>Player</th>
              <th>Date</th>
              <th>Time(s)</th>
            </tr>
          </thead>
          <tbody>
            {records.slice(0, 10).map((item, i) => (
              <tr key={i}>
                <td>{i + 1}.</td>
                <td>{item.name}</td>
                <td>{item.date}</td>
                <td>{item.time}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <div></div>
    </React.Fragment>
  );
};
export default Records;
