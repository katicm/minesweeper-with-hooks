import React from "react";
import Smiley from "./Smiley";
import bomb from "../media/bomb60x60.png";
import timer from "../media/time.png";

const GameCenter = ({ type, flagNum, time, newGame }) => {
  return (
    <div className="panel_game">
      <img className="icons" src={bomb} alt="B"></img>
      <input
        style={{ width: "60px" }}
        className="text_box"
        value={flagNum}
        disabled
      ></input>
      <Smiley type={type} newGame={newGame} />
      <input className="text_box" value={time} disabled></input>
      <img className="icons" src={timer} alt="B"></img>
    </div>
  );
};
export default GameCenter;
