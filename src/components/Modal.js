import React, { useState, useEffect, useRef, useContext } from "react";
import Popup from "reactjs-popup";
import axios from "axios";
import { ParamContext } from "../App";

const Modal = ({ type, time }) => {
  const nameRef = useRef("");
  const [open, setOpen] = useState(false);
  const { parameters } = useContext(ParamContext);

  useEffect(() => {
    if (type === "won") {
      setOpen(true);
    }
  }, [type]);
  const handleSubmit = () => {
    setOpen(false);
    nameRef.current.focus();
    if (nameRef.current.value !== "") {
      axios.post(
        `https://minesweeper-records.firebaseio.com/records/${parameters.mode}.json`,
        {
          name: nameRef.current.value,
          date: new Date().toLocaleDateString(),
          time: time
        }
      );
    }
  };

  return (
    <Popup
      contentStyle={{
        width: "250px",
        border: "3px solid #312f30",
        padding: "2px",
        height: "150px",
        borderRadius: "5px"
      }}
      open={open}
      closeOnDocumentClick
      onClose={() => {}}
    >
      <div className="modal">
        <div className="header">Congratulations, You Won!</div>
        <div className="body">
          Your time is {time}, Amazing!
          <input
            className="modal_input"
            ref={nameRef}
            placeholder="Enter Your name"
          ></input>
        </div>
        <div className="footer">
          <button
            onClick={handleSubmit}
            className="modal_button"
            style={{ backgroundColor: "#28a2c7" }}
          >
            Send
          </button>
          <button onClick={() => setOpen(false)} className="modal_button">
            Cancel
          </button>
        </div>
      </div>
    </Popup>
  );
};
export default Modal;
