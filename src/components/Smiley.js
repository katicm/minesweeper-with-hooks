import React from "react";
import run from "../media/happy.png";
import click from "../media/click.png";
import lost from "../media/dead.png";
import won from "../media/won.png";

const ENUM_SMILEY = {
  run: <img className="icons" src={run} alt="B"></img>,
  click: <img className="icons" src={click} alt="B"></img>,
  lost: <img className="icons" src={lost} alt="B"></img>,
  won: <img className="icons" src={won} alt="B"></img>
};

function Smiley({ type, newGame }) {
  return (
    <button onClick={newGame} className="smiley">
      {ENUM_SMILEY[type]}
    </button>
  );
}
export default Smiley;
