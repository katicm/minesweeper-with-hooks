import React, { useState, createContext } from "react";
import "./App.css";
import { useRoutes, A } from "hookrouter";
import Board from "./components/Board";
import Records from "./components/Records";
import Settings from "./components/Settings";
import wbomb from "./media/wbomb.png";
import ranking from "./media/ranking.png";
import settings from "./media/settings.png";

export const ParamContext = createContext(null);
const routes = {
  "/": () => <Board />,
  "/settings": () => <Settings />,
  "/records": () => <Records />
};

const App = () => {
  const router = useRoutes(routes);
  const [parameters, setParameters] = useState({
    xSize: 9,
    ySize: 9,
    bombNum: 10,
    mode: "beginner"
  });

  return (
    <ParamContext.Provider value={{ parameters, setParameters }}>
      <div className="container">
        <A className="bomb_image" href="/">
          <img src={wbomb} alt="W"></img>
        </A>
        <A className="header_menu" href="/records">
          <img
            style={{ paddingBottom: "5px", paddingRight: "5px" }}
            src={ranking}
            alt="R"
          ></img>
          RECORDS
        </A>
        <A className="header_menu" href="/settings">
          <img style={{ paddingRight: "5px" }} src={settings} alt="S"></img>{" "}
          SETTINGS
        </A>
      </div>
      {router}
    </ParamContext.Provider>
  );
};

export default App;
